// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "VFBPLibrary.generated.h"
 
UCLASS()
class VFLAB_06_API UVFBPLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintPure, Category = "VF BP Library",meta=(WorldContext="WorldContextObject" ))
	static void GetCoverPoints(UObject* WorldContextObject, TArray<FVector>& Points, float Radius=128);
	
	
};
